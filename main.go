package main

import (
	"fmt"

	"gitlab.com/rayli/test-ci/cmd"
)

func main() {
	cmd.Execute()

	fmt.Println(Sub(2, 1))
}

func Sum(a, b int) int {
	return a + b
}

func Sub(a, b int) int {
	return a - b
}
